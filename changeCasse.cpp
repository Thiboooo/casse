// changeCasse.cpp
// c++ -std=c++11 -Wall -Wextra -o changeCasse2.out changeCasse2.cpp
#include <cctype>
#include <iostream>
int main(int argc, char ** argv)
{
    if (argc != 2)
    {
        std::cerr << "usage: " << argv[0] << " <text>" << std::endl;
        exit(-1);
    }
    std::string texte = argv[1];
    for (char & c : texte)
    {
        if (std::islower(c))
            std::cout << char(std::toupper(c));
        else if (std::isupper(c))
            std::cout << char(std::tolower(c));
        else 
            std::cout << c;
    }
    return 0;
}